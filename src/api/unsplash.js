import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: 'Client-ID 5dcc908803e26250fab624bc82f4e4f909bdd013aea404680eed5d523f55b786'
  }
});