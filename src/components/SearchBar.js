import React from 'react';

class SearchBar extends React.Component {
  // onInputChange(event) {
  //   console.log(event.target.value);
  // }
  state = { term: '' };

  onFormSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmit(this.state.term);
  }
  
  render() {
    return (
      <div className="ui segment">
        <form onSubmit={this.onFormSubmit} className="ui form">
          <div className="field">
            <label>Image Search</label>
            {/* One thing to remember here is that the function shouldn't be called with parenthesis as () 
            as it will be called every time Component is rendered.
            Also, the e here is same as the event object that we used before.  */}
            <input type="text" value={this.state.term} onChange={e => this.setState({term: e.target.value})}></input>
          </div>          
        </form>
      </div>
    );
  }
}

export default SearchBar;