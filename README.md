# Important Points To Note

  - The application structure should always have a `components` folder inside `src`          directory which holds all the components in one place.
  - The default behaviour of a form is to submit the page whenever enter is triggered. In    our we don't want it and hence `preventDefault` is used in searchbar component. 
  - Under SearchBar component, the function onFormSubmit is an arrow function and not a      normal function. The reason for this is as below: 
    - You can notice we have used `this.state.term` in `console.log` function. Now if this   was a normal function than it would return `variable state not found for undefined`.   This is because a normal function has a different value of `this` than the parent      function on which it was called. 
    - Hence, to resolve this we use arrow functions as they don't have their own             defination of `this`. It's the same on which the function was called.
    - More info can be found in the below links: 
      1. https://medium.com/@thejasonfile/es5-functions-vs-es6-fat-arrow-functions-864033baa1a
      2. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions
      3. https://stackoverflow.com/questions/34361379/are-arrow-functions-and-functions-equivalent-exchangeable
  - The `async` and `await` keywords are same as using `then` function for an ajax call.     The meaning of await is basically it waits for the async `function x` until it is        returns a response. The Javascript function execution is stoped until this async         function is completed. More info can be found on below links:
    1. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
    2. https://javascript.info/async-await

  - When defining a list of elements, we should use `key` as an attribute to define each     element uniquely so that when react re-renders the page, it only inserts those           elements which are newly added.  
  - `imageRef` is javascript function of react which allows to load all dom properties of    a particular element. For example, to get height of an image in console we need to       write `jquery.querySelector('img').clientHeight()` to get an image's height. But in      react we can access from imageRef directly. 

